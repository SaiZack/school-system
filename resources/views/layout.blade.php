<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>School System - @yield('title')</title>
        <script src="https://kit.fontawesome.com/59306e82f2.js" crossorigin="anonymous"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="{{url('/css/app.css')}}">
    </head>
    <body class="d-flex">
        <div class="d-flex flex-column flex-shrink-0 text-white sidenav vh-100" id="sideNav">
            <a href="#" class="d-flex flex-column align-items-center mb-3 mb-md-0 me-md-auto text-white p-3"  onclick="openNav()">
                <i class="fa-solid fa-school-flag fs-2"></i>
                <span class="fs-3 px-3"> School System</span>
            </a>
            <div class="px-3">
                <hr>
            </div>
            <ul class="sidebar-list">
                <li class="px-3 py-2 my-2 me-1">
                    <a href="#" class="text-white">
                    <i class="fa-solid fa-user fa-lg"></i> <span class="px-3 fs-5">User</span> 
                    </a>
                </li>
                <li class="px-3 py-2 my-2 me-1">
                    <a href="#" class="text-white">
                    <i class="fa-solid fa-id-card fa-lg"></i> <span class="px-3 fs-5">User Info</span> 
                    </a>
                </li>
                <li class="px-3 py-2 my-2 me-1">
                    <a href="{{route('class.index')}}" class="text-white">
                    <i class="fa-solid fa-door-open fa-lg"></i> <span class="px-3 fs-5">Class</span> 
                    </a>
                </li>
                <li class="px-3 py-2 my-2 me-1">
                    <a href="#" class="text-white">
                    <i class="fa-solid fa-chalkboard-user fa-lg"></i> <span class="px-3 fs-5">Role</span> 
                    </a>
                </li>
                <li class="px-3 py-2 my-2 me-1">
                    <a href="#" class="text-white">
                    <i class="fa-solid fa-users-gear fa-lg"></i> <span class="px-3 fs-5">Type</span> 
                    </a>
                </li>
            </ul>
            
        </div>
        <div class="main">
            <div class="content">
                @yield('content')
            </div>
        </div>
    </body>
    <script src="{{url('/js/app.js')}}"></script>
</html>
